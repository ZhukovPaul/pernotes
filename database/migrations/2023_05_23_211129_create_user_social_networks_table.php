<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('user_social_networks', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            //$table->unsignedBigInteger('social_network_id');
            //$table->foreign('social_network_id')->references('id')->on('social_networks');
            $table->string('social_network');
            $table->string('access_token');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_social_networks');
    }
};

<?php

declare(strict_types=1);

namespace Domain\DTO\Services;

final class AccessTokenResponse
{
    public function __construct(
        public readonly string $accessToken,
        public readonly ?string $expiresIn
    ) {
    }
}

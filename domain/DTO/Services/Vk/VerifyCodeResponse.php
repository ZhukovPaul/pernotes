<?php

declare(strict_types=1);

namespace Domain\DTO\Services\Vk;

final class VerifyCodeResponse
{
    public function __construct(public readonly string $code)
    {
    }
}

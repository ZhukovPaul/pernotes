<?php

declare(strict_types=1);

namespace Domain\DTO\Services;

final class UserVerifyLinkResponse
{
    public function __construct(readonly string $link)
    {
    }
}

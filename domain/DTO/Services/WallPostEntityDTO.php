<?php

declare(strict_types=1);

namespace Domain\DTO\Services;

use Carbon\Carbon;
use Illuminate\Support\Collection;

final class WallPostEntityDTO
{
    /**
     * @param Carbon $date
     * @param string $type
     * @param int $likesCount
     * @param int $repostsCount
     * @param int $viewsCount
     * @param string $text
     * @param Collection<string,mixed> $attachments
     */
    public function __construct(
        public readonly Carbon     $date,
        public readonly string     $type,
        public readonly int        $likesCount,
        public readonly int        $repostsCount,
        public readonly int        $viewsCount,
        public readonly string     $text,
        public readonly Collection $attachments,
    ) {

    }

}

<?php

declare(strict_types=1);

namespace Domain\DTO\Services;

use Carbon\Carbon;
use Illuminate\Support\Collection;

final class WallEntityResponse
{
    /**
     * @param int $id
     * @param int $likesCount
     * @param int $repostsCount
     * @param int $views
     * @param Carbon $date
     * @param Collection<string,mixed> $comments
     * @param string $text
     * @param Collection<string,mixed> $attachments
     */
    public function __construct(
        public readonly int $id,
        public readonly int $likesCount,
        public readonly int $repostsCount,
        public readonly int $views,
        public readonly Carbon $date,
        public readonly Collection $comments,
        public readonly string $text,
        public readonly Collection $attachments,
    ) {
    }
}

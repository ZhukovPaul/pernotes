<?php

declare(strict_types=1);

namespace Domain\DTO\User;

final class UserCreateRequest
{
    public function __construct(
        public readonly string $name,
        public readonly string $lastName,
        public readonly string $email,
        public readonly string $password
    ) {
    }
}

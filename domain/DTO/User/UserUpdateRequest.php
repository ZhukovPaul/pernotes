<?php

declare(strict_types=1);

namespace Domain\DTO\User;

final class UserUpdateRequest
{
    public function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly string $lastName,
        public readonly ?int $phone,
        public readonly string $email
    ) {
    }
}

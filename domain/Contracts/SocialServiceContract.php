<?php

declare(strict_types=1);

namespace Domain\Contracts;

use Illuminate\Support\Collection;

interface SocialServiceContract
{
    public function getServiceType(): string;

    public function setAccessToken(string $token): self;

    /**
     * @return Collection<string,mixed>
     */
    public function getPosts(): Collection;
}

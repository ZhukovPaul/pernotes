<?php

declare(strict_types=1);

namespace Domain\Contracts;

use Domain\DTO\Services\AccessTokenResponse;
use Domain\DTO\Services\UserVerifyLinkResponse;

interface RegisterServiceContract
{
    public function getVerifyLink(): UserVerifyLinkResponse;

    public function getAccessToken(string $code): AccessTokenResponse;

}

<?php

declare(strict_types=1);

namespace Domain\ValueObject;

class AttachmentValueObject
{
    public function __construct(public readonly string $type)
    {

    }
}

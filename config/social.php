<?php

return [
    'vk_app_id' => env('VK_APP_ID'),
    'vk_redirect_page' => env('VK_APP_REDIRECT_PAGE'),
    'vk_secret' => env('VK_APP_SECRET'),

    'instagram_app_id' => env('INSTAGRAM_APP_ID'),
    'instagram_redirect_page' => env('INSTAGRAM_APP_REDIRECT_PAGE'),
    'instagram_secret' => env('INSTAGRAM_APP_SECRET'),
];

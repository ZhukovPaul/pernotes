<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    use DatabaseMigrations;

    /* @test */
    public function testCreateUserTest(): void
    {
        $user = Factory::create('user', [
            'name' => 'Jonh',
            'last_name' => 'Simens'
        ]);

        $this->assertFalse($user->name == 'Devid');
    }

    public function testFullName(): void
    {
        $user = new User([
            'name' => 'john',
            'last_name' => 'dou'
        ]);

        $this->assertEquals('John Dou', $user->full_name, 'Full name is not right');
    }

    public function testNameMutator(): void
    {
        $user = new User([
            'name' => 'john',
            'last_name' => 'dou'
        ]);

        $this->assertEquals('John', $user->name);
    }

    public function testLastNameMutator(): void
    {
        $user = new User([
            'name' => 'john',
            'last_name' => 'dou'
        ]);

        $this->assertEquals('Dou', $user->last_name);
    }

}

<?php

declare(strict_types=1);

namespace App\Enums;

use Domain\Contracts\SocialServiceContract;
use App\Services\Social\InstagramFacade;
use App\Services\Social\VkFacade;

enum SocialServices: string
{
    case VK = 'VK';

    case INST = 'Instagram';

    case FB = 'Facebook';

    public static function getServiceFacade(string $service): ?SocialServiceContract
    {
        return match ($service) {
            self::VK->value => app(VkFacade::class),
            self::INST->value => app(InstagramFacade::class),

            default => null
        };
    }
}

<?php

declare(strict_types=1);

namespace App\Actions\Vk;

use Domain\DTO\Services\Vk\VerifyCodeResponse;
use App\Enums\SocialServices;
use App\Repositories\UserSocialNetworkRepository;
use App\Services\Social\VkRegisterService;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\NoReturn;

final class CreateUserTokenAction
{
    public function __construct(
        private readonly VkRegisterService           $vkService,
        private readonly UserSocialNetworkRepository $userSocialNetworkRepository,
        private readonly Request                     $request
    ) {
    }

    public function __invoke(VerifyCodeResponse $codeResponse): void
    {
        $tokenDto = $this->vkService->getAccessToken($codeResponse->code);

        $this->userSocialNetworkRepository->add(
            $this->request->user()->id,
            SocialServices::VK,
            $tokenDto->accessToken
        );
    }
}

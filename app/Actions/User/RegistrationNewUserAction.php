<?php

declare(strict_types=1);

namespace App\Actions\User;

use Domain\DTO\User\UserCreateRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

final class RegistrationNewUserAction
{
    public function __invoke(UserCreateRequest $data): void
    {
        $user = new User();

        $user->update([
            'name' => $data->name,
            'last_name' => $data->lastName,
            'email' => $data->email,
            'password' => Hash::make($data->password),
        ]);

        event(new Registered($user));

        Auth::login($user);
    }
}

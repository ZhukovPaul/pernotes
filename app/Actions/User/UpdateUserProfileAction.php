<?php

declare(strict_types=1);

namespace App\Actions\User;

use Domain\DTO\User\UserUpdateRequest;
use App\Repositories\UserRepository;

class UpdateUserProfileAction
{
    public function __construct(
        private readonly UserRepository $userRepository
    ) {
    }

    public function __invoke(UserUpdateRequest $data): void
    {
        $user = $this->userRepository->getById($data->id);

        $user->update([
            'name' => $data->name,
            'last_name' => $data->lastName,
            'phone' => $data->phone,

        ]);

        if ($user->isDirty('email')) {
            $user->email_verified_at = null;
        }

        $user->save();
    }
}

<?php

declare(strict_types=1);

namespace App\Actions\Instagram;

use Domain\DTO\Services\Instagram\VerifyCodeResponse;
use App\Enums\SocialServices;
use App\Repositories\UserSocialNetworkRepository;
use App\Services\Social\InstagramRegisterService;
use Illuminate\Http\Request;

final class CreateUserTokenAction
{
    public function __construct(
        private readonly InstagramRegisterService    $instagramRegisterService,
        private readonly UserSocialNetworkRepository $userSocialNetworkRepository,
        private readonly Request                     $request
    ) {

    }

    public function __invoke(VerifyCodeResponse $codeResponse): void
    {

        $tokenDto = $this->instagramRegisterService->getAccessToken($codeResponse->code);

        $this->userSocialNetworkRepository->add(
            $this->request->user()->id,
            SocialServices::INST,
            $tokenDto->accessToken
        );
    }
}

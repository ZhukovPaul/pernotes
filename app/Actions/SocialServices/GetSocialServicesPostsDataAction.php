<?php

declare(strict_types=1);

namespace App\Actions\SocialServices;

use Domain\Contracts\SocialServiceContract;
use App\Enums\SocialServices;
use App\Models\UserSocialNetwork;
use App\Repositories\UserSocialNetworkRepository;
use Illuminate\Support\Collection;

final class GetSocialServicesPostsDataAction
{
    public function __construct(private readonly UserSocialNetworkRepository $userSocialNetworkRepository)
    {
    }

    /**
     * @param int $id
     * @return Collection<string, Collection<string,mixed>>
     */
    public function __invoke(int $id): Collection
    {
        $userSocialsNetworksCollection = $this->userSocialNetworkRepository->getByUserId($id);

        /* @var Collection<int, SocialServiceContract> $userServicesFacades */

        $userServicesFacades = $userSocialsNetworksCollection->map(
            static fn (UserSocialNetwork $item) => SocialServices::getServiceFacade($item->social_network)
                ?->setAccessToken($item->access_token)
        );

        return $userServicesFacades->mapWithKeys(static fn (SocialServiceContract $facade) => [
            $facade->getServiceType() => $facade->getPosts()
        ]);
    }
}

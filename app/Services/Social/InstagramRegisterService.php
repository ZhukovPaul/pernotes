<?php

declare(strict_types=1);

namespace App\Services\Social;

use Domain\DTO\Services\AccessTokenResponse;
use Domain\DTO\Services\UserVerifyLinkResponse;
use Domain\Contracts\RegisterServiceContract;
use GuzzleHttp\Client;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Http;

final class InstagramRegisterService implements RegisterServiceContract
{
    private const RESPONSE_TYPE = 'code';

    //private string $accessTokenUrl = 'https://www.facebook.com/v17.0/dialog/oauth';
    private string $accessTokenUrl = 'https://graph.facebook.com/oauth/access_token';

    private string $codeUrl = 'https://graph.facebook.com/oauth/authorize';

    private readonly int $clientId;

    private readonly string $redirectUrl;

    private readonly string $secret;

    private readonly string $proxy;


    public function __construct()
    {
        $this->clientId = (int)config('social.instagram_app_id');
        $this->redirectUrl = config('social.instagram_redirect_page');
        $this->secret = config('social.instagram_secret');
        $this->proxy = config('network.proxy_server');
    }

    public function getVerifyLink(): UserVerifyLinkResponse
    {
        $query = \http_build_query([
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUrl,
            'response_type' => self::RESPONSE_TYPE,
        ]);

        return new UserVerifyLinkResponse($this->codeUrl . '?' . $query);
    }

    /**
     * @throws AuthorizationException
     */
    public function getAccessToken(string $code): AccessTokenResponse
    {

        $options = $this->proxy ? ['proxy' => $this->proxy] : [];

        $response = Http::withOptions($options)
            ->asForm()
            ->post(
                $this->accessTokenUrl,
                [
                    'client_id' => $this->clientId,
                    'client_secret' => $this->secret,
                    'code' => $code,
                    'redirect_uri' => $this->redirectUrl
                ]
            )->collect();
        //dd($response);
        if ($response->has('error')) {
            throw new AuthorizationException($response->get('error')['message']);
        }

        return new AccessTokenResponse($response->get('access_token'), $response->get('5172726'));
    }
}

<?php

declare(strict_types=1);

namespace App\Services\Social;

use Domain\Contracts\SocialServiceContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

final class InstagramFacade implements SocialServiceContract
{
    private string $accessToken;

    public function setAccessToken(string $token): SocialServiceContract
    {
        $this->accessToken = $token;

        return $this;
    }

    public function getPosts(): Collection
    {
        $response = Http::withOptions([
            'proxy' => config('network.proxy_server')
        ])
            ->asForm()
            ->get(
                'https://graph.instagram.com/me/media',
                [
                    'fields' => 'id,media_type,media_url,caption,timestamp,thumbnail_url,permalink',
                    'access_token' => $this->accessToken,
                ]
            )->collect();


        return  $response->collect();
    }

    public function getServiceType(): string
    {
        return 'Instagram';
    }
}

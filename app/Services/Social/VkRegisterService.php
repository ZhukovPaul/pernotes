<?php

declare(strict_types=1);

namespace App\Services\Social;

use Domain\Contracts\RegisterServiceContract;
use Domain\DTO\Services\AccessTokenResponse;
use Domain\DTO\Services\UserVerifyLinkResponse;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

final class VkRegisterService implements RegisterServiceContract
{
    private readonly int $clientId;

    private readonly string $redirectUrl;

    private readonly string $secret;

    public function __construct(readonly VKOAuth $vkOAuth)
    {
        $this->clientId = (int)config('social.vk_app_id');
        $this->redirectUrl = config('social.vk_redirect_page');
        $this->secret = config('social.vk_secret');
    }

    public function getVerifyLink(): UserVerifyLinkResponse
    {
        $display = VKOAuthDisplay::PAGE;

        $scope = [
            VKOAuthUserScope::WALL,
            VKOAuthUserScope::GROUPS,
            VKOAuthUserScope::PHOTOS,
            VKOAuthUserScope::FRIENDS,
            VKOAuthUserScope::STATS,
            VKOAuthUserScope::NOTIFICATIONS,
            VKOAuthUserScope::STATUS,
            VKOAuthUserScope::OFFLINE,
            VKOAuthUserScope::LINK,
            VKOAuthUserScope::NOTIFY
        ];

        $link = $this->vkOAuth->getAuthorizeUrl(
            VKOAuthResponseType::CODE,
            $this->clientId,
            $this->redirectUrl,
            $display,
            $scope,
            $this->secret
        );

        return new UserVerifyLinkResponse(link: $link);
    }

    public function getAccessToken(string $code): AccessTokenResponse
    {
        $response = $this->vkOAuth->getAccessToken(
            $this->clientId,
            $this->secret,
            $this->redirectUrl,
            $code
        );

        return new AccessTokenResponse($response['access_token'], null);
    }
}

<?php

declare(strict_types=1);

namespace App\Services\Social;

use Domain\Contracts\SocialServiceContract;
use Domain\DTO\Services\WallPostEntityDTO;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use VK\Enums\UsersFields;

use VK\Client\VKApiClient;
use VK\Exceptions\Api\VKApiBlockedException;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

final class VkFacade implements SocialServiceContract
{
    private string $accessToken;

    public function __construct(readonly VKApiClient $VKApiClient)
    {
    }

    public function getUserProfile(): mixed
    {
        $userProfile = $this->VKApiClient
            ->users()
            ->get($this->accessToken, [
                'fields' => [
                    UsersFields::BDATE,
                    UsersFields::PHOTO_400_ORIG,
                    UsersFields::CITY,
                    UsersFields::COUNTRY,
                    UsersFields::COUNTERS,
                    UsersFields::SCREEN_NAME,
                    UsersFields::CAN_WRITE_PRIVATE_MESSAGE,
                    UsersFields::CAN_WRITE_PRIVATE_MESSAGE
                ]
            ]);
        return collect($userProfile)->first();
    }

    /**
     * @throws VKApiException
     * @throws VKApiBlockedException
     * @throws VKClientException
     */
    public function getPosts(): Collection
    {
        $response = $this->VKApiClient->wall()->get($this->accessToken, [
            'count' => 10,
           // 'filter' => WallFilter::OWNER,
        ]);

        return collect($response['items'])->map(
            static fn ($entity) => new WallPostEntityDTO(
                date: Carbon::parse($entity['date']),
                type: $entity['type'],
                likesCount: (int)$entity['likes']['count'],
                repostsCount: (int)$entity['reposts']['count'],
                viewsCount: (int)$entity['views']['count'],
                text: isset($entity['copy_history'])
                    ? $entity['copy_history'][0]['text'] : $entity['text'],
                attachments:  isset($entity['copy_history'][0]['attachments'])
                    ? collect($entity['copy_history'][0]['attachments'])
                    : collect($entity['attachments'])
            )
        );
    }

    public function setAccessToken(string $token): SocialServiceContract
    {
        $this->accessToken = $token;

        return $this;
    }

    public function getServiceType(): string
    {
        return 'VK';
    }
}

<?php

declare(strict_types=1);

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\AsCollection;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @method static create(array $array)
 */
class User extends Authenticatable implements HasMedia
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use InteractsWithMedia;


    /**
     * @var mixed|null
     */
    public mixed $email_verified_at;

    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'phone',
        'user_settings'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'user_settings' => AsCollection::class,
    ];

    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn(string $value) => Str::ucfirst($value)
        );
    }

    protected function lastName(): Attribute
    {
        return Attribute::make(
            get: fn(string $value) => Str::ucfirst($value)
        );
    }

    protected function fullName(): Attribute
    {
        return Attribute::make(
            get: fn() => sprintf('%s %s', $this->name, $this->last_name),
        );
    }

    public function social(): HasMany
    {
        return $this->hasMany(UserSocialNetwork::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property  string $social_network;
 * @property  string $access_token;
 */

class UserSocialNetwork extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'social_network', 'access_token'];

    //public string $access_token;

    //public string $social_network;
}

<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Domain\DTO\User\UserCreateRequest as UserCreateRequestDto;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

final class UserCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'lastName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', 'confirmed', Password::defaults()],
        ];
    }

    public function getDto(): UserCreateRequestDto
    {
        $this->validated();

        return new UserCreateRequestDto(
            name: $this->input('name'),
            lastName: $this->input('lastName'),
            email: $this->input('email'),
            password: $this->input('password')
        );
    }
}

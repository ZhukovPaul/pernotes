<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Domain\DTO\User\UserUpdateRequest;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['required'],
            'name' => ['string', 'max:255', 'required'],
            'last_name' => ['string', 'max:255', 'required'],
            'phone' => ['string', 'max:255', 'nullable'],
            'email' => ['email', 'max:255'],
        ];
    }

    public function getDto(): UserUpdateRequest
    {
        $data = $this->validated();

        return new UserUpdateRequest(
            id: (int)$data['id'],
            name: $data['name'],
            lastName: $data['last_name'],
            phone: (int)$data['phone'],
            email: $data['email'],
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Domain\DTO\Services\Vk\VerifyCodeResponse;
use Illuminate\Foundation\Http\FormRequest;

final class VkServiceCodeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'code' => 'string'
        ];
    }

    public function getDto(): VerifyCodeResponse
    {
        $this->validated();

        return new VerifyCodeResponse(
            code: $this->input('code'),
        );
    }
}

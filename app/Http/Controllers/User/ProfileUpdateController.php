<?php

declare(strict_types=1);

namespace App\Http\Controllers\User;

use App\Actions\User\UpdateUserProfileAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\Response;

class ProfileUpdateController extends Controller
{
    public function __invoke(
        ProfileUpdateRequest    $profileUpdateRequest,
        UpdateUserProfileAction $updateUserProfileAction
    ): Response {
        $updateUserProfileAction($profileUpdateRequest->getDto());

        return response('OK', 200);
    }
}

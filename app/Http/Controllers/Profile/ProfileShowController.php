<?php

declare(strict_types=1);

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Services\Social\InstagramRegisterService;
use App\Services\Social\VkRegisterService;
use Illuminate\Http\Request;
use Illuminate\View\View;

final class ProfileShowController extends Controller
{
    public function __invoke(
        Request                          $request,
        VkRegisterService                $vkService,
        InstagramRegisterService         $instagramRegisterService
    ): View {

        return view('profile.edit', [
            'user' => $request->user(),
            'vk_register_link' => $vkService->getVerifyLink(),
            'instagram_register_link' => $instagramRegisterService->getVerifyLink()
        ]);
    }
}

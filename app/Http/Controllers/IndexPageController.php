<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\SocialServices\GetSocialServicesPostsDataAction;
use Illuminate\View\View;
use Illuminate\Http\Request;

class IndexPageController extends Controller
{
    /**
     *
     * @param Request $request
     * @param GetSocialServicesPostsDataAction $getSocialServicesPostsDataAction
     * @return  View
     */
    public function __invoke(
        Request                          $request,
        GetSocialServicesPostsDataAction $getSocialServicesPostsDataAction
    ): View {

        return view('index');
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Actions\User\RegistrationNewUserAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    public function create(): View
    {
        return view('auth.register');
    }

    public function store(
        UserCreateRequest         $request,
        RegistrationNewUserAction $registrationNewUserAction
    ): RedirectResponse {
        $registrationNewUserAction($request->getDto());

        return redirect(RouteServiceProvider::HOME);
    }
}

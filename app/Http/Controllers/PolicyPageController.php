<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;

class PolicyPageController extends Controller
{
    /**
     * @return  View
     */
    public function __invoke(Request $request): View
    {
        return view('pages.policy');
    }
}

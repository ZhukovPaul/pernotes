<?php

declare(strict_types=1);

namespace App\Http\Controllers\Services\Vk;

use App\Actions\Vk\CreateUserTokenAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\VkServiceCodeRequest;
use Illuminate\Http\RedirectResponse;

final class CreateAccessTokenController extends Controller
{
    public function __invoke(
        VkServiceCodeRequest $request,
        CreateUserTokenAction $createUserTokenAction
    ): RedirectResponse {
        $createUserTokenAction($request->getDto());

        return redirect()->route('index');
    }
}

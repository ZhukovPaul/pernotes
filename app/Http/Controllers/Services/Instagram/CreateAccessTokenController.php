<?php

declare(strict_types=1);

namespace App\Http\Controllers\Services\Instagram;

use App\Actions\Instagram\CreateUserTokenAction;
use App\Http\Requests\InstagramServiceCodeRequest;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;

class CreateAccessTokenController extends Controller
{
    public function __invoke(
        InstagramServiceCodeRequest $request,
        CreateUserTokenAction $createUserTokenAction
    ): RedirectResponse {

        $createUserTokenAction($request->getDto());

        return redirect()->route('index');
    }
}

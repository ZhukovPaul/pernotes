<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Enums\SocialServices;
use App\Models\UserSocialNetwork;
use Illuminate\Support\Collection;

final class UserSocialNetworkRepository
{
    /*
     * return UserSocialNetwork
     */
    public function add(int $userId, SocialServices $socialNetwork, string $token): UserSocialNetwork
    {
        return UserSocialNetwork::create([
            'user_id' => $userId,
            'social_network' => $socialNetwork,
            'access_token' => $token
        ]);
    }

    /**
     * @param int $id
     * @return Collection<UserSocialNetwork>
     */
    public function getByUserId(int $id): Collection
    {
        return  UserSocialNetwork::query()
            ->where('user_id', '=', $id)
            ->get();
    }
}

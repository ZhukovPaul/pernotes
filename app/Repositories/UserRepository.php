<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\User;
use Couchbase\CollectionSpec;
use Illuminate\Support\Collection;

final class UserRepository
{
    public function getById(int $id): ?User
    {
        return User::query()->where('id', '=', $id)?->first();
    }
}

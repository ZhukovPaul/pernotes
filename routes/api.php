<?php

use App\Actions\SocialServices\GetSocialServicesPostsDataAction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Services\Social\VkRegisterService;
use App\Services\Social\InstagramRegisterService;
use App\Http\Controllers\User\ProfileUpdateController;


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('v1')->group(function () {

    /**
     * TODO refactoring
     */
    Route::prefix('/users/')->group(function () {
        Route::get('{id}', function (\App\Repositories\UserRepository $userRepository, string $id) {
            return new \App\Http\Resources\Api\UserResource($userRepository->getById($id));
        });

        Route::put('{id}', ProfileUpdateController::class);
    });

    /**
     * TODO refactoring
     */
    Route::get('posts/{id}', function (
        string                           $id,
        GetSocialServicesPostsDataAction $getSocialServicesPostsDataAction
    ) {

        return $getSocialServicesPostsDataAction($id);
    });

    /**
     * TODO refactoring
     */
    Route::get('/links/', function (
        VkRegisterService $registerService, InstagramRegisterService $instagramRegisterService
    ) {

        return [
            'vk' => $registerService->getVerifyLink()->link,
            'instagram' => $instagramRegisterService->getVerifyLink()->link
        ];
    });
});




<?php


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\IndexPageController;
use Telegram\Bot\Laravel\Facades\Telegram;
use \App\Http\Controllers\Profile\ProfileShowController;
use App\Http\Controllers\Services\Vk\CreateAccessTokenController as VkCreateAccessTokenController;
use App\Http\Controllers\Services\Instagram\CreateAccessTokenController as InstagramCreateAccessTokenController;

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/policy/', \App\Http\Controllers\PolicyPageController::class);

//Route::get('/telegramv2/',  \App\Http\Controllers\TelegramController::class);
/*Route::get('/telegram/', function (\Telegram\Bot\Api $telegram, \Illuminate\Support\Facades\Http $http){

    $response = Http::withOptions([
        'proxy' => 'http://24007289:1d5b5fb432ac7a6ca8d8e7f1bb7d8c47@149.154.167.40:443'
    ])
        ->asForm()
        ->get('https://api.telegram.org/bot6693072575:AAEeSTuMVSS_ZcIFSrIfX_xutN3zZTPfID4/messages.getHistory',
    [
        //'api_id' => 24007289,
        //'api_hash' => '1d5b5fb432ac7a6ca8d8e7f1bb7d8c47',
        //'bot_auth_token' => ''
        'peer' => '@interesting_blog'

    ])->json();

    dd($response);

   // $messageId = $response->getMessageId();

    dd($botId, $firstName, $username);
});
*/
require __DIR__.'/auth.php';



Route::middleware('auth')->group(function () {

    Route::get('/{page}', IndexPageController::class);
    Route::get('/', IndexPageController::class)->name('index');

    Route::get('/vk/access/',VkCreateAccessTokenController::class);
    Route::get('/instagram/access/',InstagramCreateAccessTokenController::class);

  //  Route::get('/profile', ProfileShowController::class)->name('profile.edit');
});





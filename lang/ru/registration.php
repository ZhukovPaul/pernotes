<?php

return [
    'already_registered' => 'Уже зарегистрированы?',
    'create_account' => 'Создать аккаунт',
    'restore_account' => 'Восстановить аккаунт',
    'confirm_password' => 'Подтвердите пароль',
];

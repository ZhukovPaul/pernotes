import {createStore} from 'vuex'
import posts from "../modules/posts";
import user from "../modules/user"

export default createStore({
    modules : {
        posts,
        user
    }
})

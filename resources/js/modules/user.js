import config from "../config";

export default {
    state() {
        return {
            user: null
        }
    },
    actions: {
        getCurUser(context) {
            axios.get('/api/' + config.apiVersion + '/users/' +config.userId).then(response => (
                context.commit('getCurUser', response.data))
            );
        },
    },
    mutations: {
        getCurUser(state, user) {
            console.log(user);
            state.user = user;
        }
    },
    getters : {
        getCurUser(state){
            return state.user;
        }
    }
}

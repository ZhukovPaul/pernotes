import config from "../config";

export default {
    state() {
        return {
            posts: [],
            statistic: {
                likes: 0, views: 0, reposts: 0
            }
        }
    },
    actions: {
        fetchPosts(context) {
            axios.get('/api/' + config.apiVersion + '/posts/' + config.userId).then(response => (
                context.commit('updatePosts', response.data))
            );
        },
        fetchStatistics: function (context) {

            axios.get('/api/' + config.apiVersion + '/posts/' + config.userId).then(response => {

                let likesCount = 0;
                let repostsCount = 0;
                let viewsCount = 0;

                let socials = Object.keys(response.data);

                socials.forEach(key => {
                    console.log(response.data[key]);
                    response.data[key].forEach(itemIn => {
                        likesCount += itemIn.likesCount;
                        repostsCount += itemIn.repostsCount;
                        viewsCount += itemIn.viewsCount;
                    });
                });


                context.commit('updateStatistic', {
                    likesCount: likesCount, repostsCount: repostsCount, viewsCount: viewsCount
                });
            });


        }

    },
    mutations: {
        updatePosts(state, posts) {
            state.posts = posts;
        },
        updateStatistic(state, stats) {

            state.statistic.likes = stats.likesCount;
            state.statistic.views = stats.viewsCount;
            state.statistic.reposts = stats.repostsCount;
        }
    },
    getters: {
        getPosts(state) {
            return state.posts;
        },
        getPostsStatistic(state) {
            return state.statistic;
        }
    }
}

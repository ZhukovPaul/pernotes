
import './bootstrap';
import Router from "./router";
import Store from "./store/index";
import App from './components/App.vue';

import { createApp } from 'vue'

const app = createApp(App)
    .use(Store)
    .use(Router)
    .mount('#app');

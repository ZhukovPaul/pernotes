import { createWebHistory, createRouter } from "vue-router";
import ProfileComponent from "./components/ProfileComponent.vue";
import IndexComponent from "./components/IndexComponent.vue";

export default  createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/profile',
            component: ProfileComponent
        },
        {
            path: '/',
            component: IndexComponent
        }
    ]
});

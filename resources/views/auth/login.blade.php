<x-guest-layout>

    <div class="d-flex justify-content-center py-4">
        <a href="index.html" class="logo d-flex align-items-center w-auto">
            <img src="assets/img/logo.png" alt="">
            <span class="d-none d-lg-block">NiceAdmin</span>
        </a>
    </div><!-- End Logo -->

    <div class="card mb-3">

        <div class="card-body">

            <div class="pt-4 pb-2">
                <h5 class="card-title text-center pb-0 fs-4">Войдите в свой аккацнт</h5>
                <p class="text-center small">Ввелите свой логин и пароль</p>
            </div>

            <form method="POST" action="{{ route('login') }}" class="row g-3 needs-validation" novalidate>
                @csrf
                <div class="col-12">
                    <label for="yourUsername" class="form-label">Логин</label>
                    <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                        <input type="email" name="email"  value="{{old('email')}}" class="form-control" id="yourUsername" required>
                        <div class="invalid-feedback">Пожалуйста введите Ваш логин.</div>
                    </div>
                </div>

                <div class="col-12">
                    <label for="yourPassword" class="form-label">Пароль</label>
                    <input  type="password"
                            name="password"
                            required autocomplete="current-password"
                            class="form-control" id="yourPassword"
                    >
                    <div class="invalid-feedback">Пожалуйста введите Ваш пароль!</div>
                </div>

                <div class="col-12">
                    <div class="form-check">
                        <input class="form-check-input"  type="checkbox"  name="remember" value="true" id="rememberMe">
                        <label class="form-check-label" for="rememberMe">Запомнить меня</label>
                    </div>
                </div>
                <div class="col-12">
                    <button class="btn btn-primary w-100" type="submit">Войти</button>
                </div>
                <div class="col-12">
                    <p class="small mb-0"> <a href="{{route('register')}}">Регистрация</a></p>
                </div>

                </div>
            </form>

        </div>
    </div>

</x-guest-layout>

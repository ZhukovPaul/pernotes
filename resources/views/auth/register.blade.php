<x-guest-layout>

    <div class="d-flex justify-content-center py-4">
        <a href="index.html" class="logo d-flex align-items-center w-auto">
            <img src="assets/img/logo.png" alt="">
            <span class="d-none d-lg-block">PerNote</span>
        </a>
    </div><!-- End Logo -->
    <div class="card mb-3">

        <div class="card-body">

            <div class="pt-4 pb-2">
                <h5 class="card-title text-center pb-0 fs-4">{{__('registration.create_account')}}</h5>
                <p class="text-center small">Введите Ваши персональные данные для создания аккаунта</p>
            </div>

            <form class="row g-3 needs-validation
                @if($errors->isNotEmpty())
                    was-validated
                @endif
                "
                  method="POST"
                  action="{{ route('register') }}" novalidate>
                @csrf
                <div class="col-12">
                    <x-input-label for="yourName">Ваше имя</x-input-label>
                    <x-text-input
                        type="text" name="name"
                        :value="old('name')"
                        required autofocus autocomplete="name"
                        id="yourName"/>
                    <div class="invalid-feedback">Пожалуйста введите Ваше имя!</div>
                    <x-input-error :messages="$errors->get('name')" class="mt-2"/>
                </div>

                <div class="col-12">
                    <x-input-label for="yourLastName">Ваша фамилия</x-input-label>
                    <x-text-input type="text"
                                  name="lastName"
                                  :value="old('lastName')"
                                  required autofocus autocomplete="lastname"
                                  id="yourLastName"/>
                    <div class="invalid-feedback">Пожалуйста введите Вашу фамилию!</div>
                    <x-input-error :messages="$errors->get('lastname')" class="mt-2"/>
                </div>

                <div class="col-12">
                    <x-input-label for="yourEmail">Ваш Email</x-input-label>
                    <x-text-input type="email" name="email" :value="old('email')"
                                  required autocomplete="email"
                                  id="yourEmail"/>
                    <div class="invalid-feedback">Пожалуйста введите Ваш email!</div>
                    <x-input-error :messages="$errors->get('email')" class="mt-2 mb-0"/>
                </div>


                <div class="col-12">
                    <x-input-label for="yourPassword" class="form-label">Пароль</x-input-label>
                    <x-text-input type="password"
                                  name="password"
                                  required autocomplete="new-password"
                                  id="yourPassword"/>
                    <div class="invalid-feedback">Пожалуйста введите Ваш пароль!</div>

                    <x-input-error :messages="$errors->get('password')" class="mt-2"/>
                </div>

                <div class="col-12">


                    <x-input-label for="password_confirmation" :value="__('registration.confirm_password')"/>
                    <x-text-input id="password_confirmation"
                                  type="password"
                                  name="password_confirmation" required autocomplete="new-password"/>
                    <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2"/>

                </div>
                <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2"/>

                <!--div class="col-12">
                    <div class="form-check">
                        <input class="form-check-input" name="terms" type="checkbox" value="" id="acceptTerms" required>
                        <label class="form-check-label" for="acceptTerms">I agree and accept the <a href="#">terms and conditions</a></label>
                        <div class="invalid-feedback">You must agree before submitting.</div>
                    </div>
                </div-->
                <div class="col-12">
                    <button class="btn btn-primary w-100" type="submit">{{ __('registration.create_account') }}</button>
                </div>
                <div class="col-12">
                    <p class="small mb-0">{{ __('registration.already_registered')}} <a href="{{ route('login') }}">Войти</a>
                    </p>
                </div>
            </form>

        </div>
    </div>

</x-guest-layout>

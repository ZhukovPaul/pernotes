<x-app-layout>
    <div class="col-xl-4">

        <div class="card">
            <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

                <img src="assets/img/profile-img.jpg" alt="Profile" class="rounded-circle">
                <h2>{{ Auth::user()->name }} {{ Auth::user()->lastName }}</h2>
                <h3>{{ Auth::user()->position }}</h3>
                <!--div class="social-links mt-2">
                    <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                    <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                    <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                </div-->
            </div>
        </div>

    </div>
    <div class="col-xl-8">

        <div class="card">
            <div class="card-body pt-3">
                <!-- Bordered Tabs -->
                <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">

                    <!--li class="nav-item" role="presentation">
                        <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview" aria-selected="true" role="tab">Краткий обзор</button>
                    </li-->

                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-edit" aria-selected="false" tabindex="-1" role="tab">Редактировать профиль</button>
                    </li>

                    <li class="nav-item" role="presentation">
                        <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-settings" aria-selected="false" tabindex="-1" role="tab">Настройки</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" data-bs-toggle="tab" data-bs-target="#integrations" aria-selected="false" tabindex="-1" role="tab">Интеграции</button>
                    </li>

                    <li class="nav-item" role="presentation">
                        <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password" aria-selected="false" tabindex="-1" role="tab">Изменить пароль</button>
                    </li>

                </ul>
                <div class="tab-content pt-2">

                    <div class=" tab-pane fade show active profile-overview  pt-3" id="profile-edit" role="tabpanel">


                    </div>


                    <div class=" tab-pane fade  integrations  pt-3" id="integrations" role="tabpanel">
                        <p> Подключить ВК <a href="{{$vk_register_link->link}}}">Вход</a></p>
                        <p>Подключить Instagram <a href="{{$instagram_register_link->link}}">Вход</a></p>

                    </div>

                    <div class="tab-pane fade pt-3" id="profile-settings" role="tabpanel">

                        <!-- Settings Form -->
                        <form>

                            <div class="row mb-3">
                                <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Email Notifications</label>
                                <div class="col-md-8 col-lg-9">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="changesMade" checked="">
                                        <label class="form-check-label" for="changesMade">
                                            Changes made to your account
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="newProducts" checked="">
                                        <label class="form-check-label" for="newProducts">
                                            Information on new products and services
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="proOffers">
                                        <label class="form-check-label" for="proOffers">
                                            Marketing and promo offers
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="securityNotify" checked="" disabled="">
                                        <label class="form-check-label" for="securityNotify">
                                            Security alerts
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </div>
                        </form><!-- End settings Form -->

                    </div>

                    <div class="tab-pane fade pt-3" id="profile-change-password" role="tabpanel">

                        <form method="post" action="{{ route('password.update') }}"  >
                            @csrf
                            @method('put')

                            <div class="row mb-3">
                                <label for="company" class="col-md-4 col-lg-3 col-form-label">{{__('Текущий пароль')}}</label>
                                <div class="col-md-8 col-lg-9">
                                    <input id="current_password" name="current_password"  type="password"  class="form-control"  value="">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="company" class="col-md-4 col-lg-3 col-form-label">{{__('Новый пароль')}}</label>
                                <div class="col-md-8 col-lg-9">
                                    <input   type="password"  class="form-control" id="password" name="password" value="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="company" class="col-md-4 col-lg-3 col-form-label">{{__('Подтвердите пароль')}}</label>
                                <div class="col-md-8 col-lg-9">
                                    <input   type="password"  class="form-control" id="password_confirmation" name="password_confirmation" value="">
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Изменить</button>
                            </div>



                        </form>


                    </div>

                </div><!-- End Bordered Tabs -->

            </div>
        </div>

    </div>

</x-app-layout>
